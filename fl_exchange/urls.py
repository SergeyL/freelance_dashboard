"""test123 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from ad.views import index, get_exchange_block, get_exchange_data

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^$', index, name='index'),
    url(r'^get_exchange_block/(?P<exchange_id>[0-9]+)/$', get_exchange_block, name='get_exchange_block'),
    url(r'^get_exchange_data/(?P<exchange_id>[0-9]+)/$', get_exchange_data, name='get_exchange_data'),
]
