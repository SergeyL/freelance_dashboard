# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-09-26 19:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ad', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='exchange',
            name='url_to_start_grab',
            field=models.CharField(default='', max_length=256),
            preserve_default=False,
        ),
    ]
