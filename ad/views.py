# coding: utf-8

# sys

# django
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext

# project
from models import Exchange, Ad
from data_manager import GetExchangeDataFromSite




def index(request):
    """
    
    
    Arguments:
    - `request`:
    """
    return render_to_response('ad/index.html',
                                  {'title': 'Выборка объявлений с рунетовских бирж'},
                                  )

def get_exchange_data(request, exchange_id):
    """
    получить данные по бирже
    """
    data_manager = GetExchangeDataFromSite()
    parser = data_manager.exchange_id_to_parser_class[int(exchange_id)]()
    return render_to_response('ad/ads_list.html', {'data': parser.get_data()})

def get_exchange_block(request, exchange_id):
    """
    отрисовать пустой блок с заголовком и местом для объявлений с биржи
    """
    exchange = Exchange.objects.get(id=exchange_id)
    return render_to_response('ad/exchange_block.html', {'exchange': exchange})
