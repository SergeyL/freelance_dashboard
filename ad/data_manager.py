# coding: utf-8
"""

"""

# sys
import datetime
from grab import Grab
from lxml.html import fromstring

# django

# project


class GetExchangeDataInterface(object):
    """
    получить обхъявления из биржи
    
    """
    
    def get_data():
        pass
        


class GetExchangeDataFromSite(GetExchangeDataInterface):
    """
    получение на прямую с сайта
    """
    
    def __init__(self, ):
        """
        
        """
        self.exchange_id_to_parser_class = {
            1: FlruParser,
            2: FreelanceruParser,
            3: WeblancernetParser
        }

    def get_data(self):
        """
        под реализацию потомками
        """
        pass


class FlruParser(GetExchangeDataFromSite):
    """
    fl.ru
    """
    
    def __init__(self, ):
        """
        
        """
        self.base_url = 'http://fl.ru'
    
    def get_data(self):
        """
        получить список объявлений
        """
        g = Grab()
        g.go('https://www.fl.ru/projects/?kind=1&page=1')
        ads_list = g.css_list('div[id^="project-item"]')
        list_ads_dicts = []
        for x in ads_list:
            list_ads_dicts.append(self.parse_ad(x))
        return sorted(list_ads_dicts, key= lambda d: d['date'], reverse=True)[:20]
        

    def parse_ad(self, ad):
        """
        разобрать элемент

        
        оч специфичные там люди работают, они через js выводят контент в частности описание и футер с датой  кол-вом коментов и тд
        во придурки
        """
        body_txt = ad.cssselect('script')[1].text.replace("document.write('", '').replace(">');", ">")
        body = fromstring(body_txt)
        
        ad_dict = {
            'title': ad.cssselect('h2 a')[0].text,
            'date': self.get_date(ad),
            'url_to_ad': '%s/%s' % (self.base_url, ad.cssselect('h2 a')[0].attrib['href']),
            'short_desc': body.cssselect('.b-post__txt')[0].text
        }
        return ad_dict

    def get_date(self, ad):
        """
        1. хтмл код содержащий подвал объявления - принтится js-ом, 
        2. сам формат даты это 
          33 минуты назад  
          1 час 1 минуту назад
          21 сентября, 15:02 - 5-я страница на первых 2-х страницах формат даты как показано 2 примера выше
        """
        footer_txt = ad.cssselect('script')[2].text.replace("document.write('", '').replace(">');", ">")
        #footer = fromstring(footer_txt)
        start_date_str = footer_txt.find(u'&nbsp;&nbsp;') + 13
        end_date_str = footer_txt.find(u'назад')
        date_txt = footer_txt[start_date_str:end_date_str]
        date_list = date_txt.strip(' ').split(' ')
        # 33 минуты назад
        curr_date = datetime.datetime.now()
        if len(date_list) == 2:
            curr_date = curr_date - datetime.timedelta(minutes=int(date_list[0]))
        elif len(date_list) == 4: #1 час 1 минуту назад
            curr_date = curr_date - datetime.timedelta(hours=int(date_list[0])) - datetime.timedelta(minutes=int(date_list[2]))
        else:
            #если дата не попарсилась вычитаем день от теущего времени
            curr_date = curr_date - datetime.timedelta(days=1)
        return curr_date
            


class FreelanceruParser(GetExchangeDataFromSite):
    """
    freelance.ru
    """
    
    def __init__(self, ):
        """
        
        """
        self.base_url = 'https://freelance.ru'

    def get_data(self):
        """
        получить список объявлений
        """
        
        g = Grab()
        g.go('https://freelance.ru/projects/filter/?specs=4&page=1')
        ads_list = g.css_list('div.projects .proj')
        list_ads_dicts = []
        for x in ads_list:
            list_ads_dicts.append(self.parse_ad(x))
        return sorted(list_ads_dicts, key= lambda d: d['date'], reverse=True)[:20]

    def parse_ad(self, ad):
        """
        разобрать объявление
        """
        tmp_date = ad.cssselect('li.pdata')[0].attrib['title'].split(' ')
        if len(tmp_date) == 4:
            tmp_str_date = "%s %s" % (tmp_date[2], tmp_date[3])
        else:
            tmp_str_date = "%s %s" % (tmp_date[1], tmp_date[2])
        ad_dict = {
            'title': ad.cssselect('a.ptitle span')[0].text,
            'date': datetime.datetime.strptime(tmp_str_date, "%d.%m.%y %H:%M"),
            'url_to_ad': '%s/%s' % (self.base_url, ad.cssselect('a.ptitle')[0].attrib['href']),
            'short_desc': ad.cssselect('a.descr span')[1].text
        }
        return ad_dict
        
        
class WeblancernetParser(GetExchangeDataFromSite):
    """
    weblancer.net
    """
    
    def __init__(self, ):
        """
        
        """
        self.base_url = 'https://weblancer.net'

    def get_data(self):
        """
        получить список объявлений
        """
        
        list_ads_dicts = []
        g = Grab()
        parse_urls = [
            'https://www.weblancer.net/jobs/?type=project&page=1'
            'https://www.weblancer.net/jobs/?type=project&page=2',
        ]
        for url in parse_urls:
            g.go(url)
            ads_list = g.css_list('div.page_content > div.container-fluid > .row')
            for x in ads_list:
                list_ads_dicts.append(self.parse_ad(x))
        return sorted(list_ads_dicts, key= lambda d: d['date'], reverse=True)[:20]

    def parse_ad(self, ad):
        """
        разобрать объявление
        """
        tmp_date = ad.cssselect('span.time_ago')[0].attrib['title'].split(' ')
        tmp_str_date = "%s %s" % (tmp_date[0], tmp_date[2])
        ad_dict = {
            'title': ad.cssselect('h2 a')[0].text,
            'date': datetime.datetime.strptime(tmp_str_date, "%d.%m.%Y %H:%M"),
            'url_to_ad': '%s/%s' % (self.base_url, ad.cssselect('h2 a')[0].attrib['href']),
            'short_desc': ad.cssselect('div.col-xs-12')[0].text
        }
        return ad_dict
        
