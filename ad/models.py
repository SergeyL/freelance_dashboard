# coding: utf-8
from __future__ import unicode_literals

#sys

#django
from django.db import models
from django.contrib import admin

#project


class Exchange(models.Model):
    """
    биржа с объявлениями
    """
    url = models.CharField(max_length=60)
    url_to_profile = models.CharField(max_length=120)
    url_to_start_grab = models.CharField(max_length=256)

class Ad(models.Model):
    """
    объявдения на биржах
    """
    url_to_ad = models.CharField(max_length=120)
    title  = models.CharField(max_length=240)
    date_time = models.DateTimeField(max_length=60)
    checked = models.BooleanField(default=False)
    view = models.BooleanField(default=False)

admin.site.register(Exchange)
admin.site.register(Ad)


