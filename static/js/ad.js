$(function(){
    (function($){

        var methods = {
            init : function( options ) {
                options = $.extend({
                    exchange_id: 1, // ид биржи
                    refresh: 10 // время обновления информации
                }, options);

                // загрузить базовый шаблон блока
                $(this).load('/get_exchange_block/'+options.exchange_id+'/', '', function(){
                    // проставить атрибуты
                    $(this).find(".exchange_title,.exchange_ads").attr('exchange_id', options.exchange_id);
                    // заполним данные
                    $(this).exchange('update');
                });

                
                //clearInterval(interval_id)                
                var interval_id = setInterval('$("#'+this.attr('id')+'").exchange("update");', options.refresh*1000);
            },
            
            update : function() {
                this.find('.exchange_ads').load('/get_exchange_data/' + this.find('.exchange_ads').attr('exchange_id')  + '/');
            }};
        
        
        jQuery.fn.exchange = function(method) {

            // логика вызова метода
            if ( methods[method] ) {
                return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
            } else if ( typeof method === 'object' || ! method ) {
                return methods.init.apply( this, arguments );
            } else {
                $.error( 'Метод с именем ' +  method + ' не существует для jQuery.exchange' );
            } 
        };
    })(jQuery);

    $('#fl').exchange({'exchange_id': 1, 'refresh': 300});
    $('#fr').exchange({'exchange_id': 2, 'refresh': 300});
    $('#weblancer').exchange({'exchange_id': 3, 'refresh': 300});
});


